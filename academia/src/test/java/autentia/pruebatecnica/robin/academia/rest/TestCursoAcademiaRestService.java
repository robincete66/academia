package autentia.pruebatecnica.robin.academia.rest;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.ibatis.session.RowBounds;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.databind.ObjectMapper;

import autentia.pruebatecnica.robin.academia.AcademiaApplication;
import autentia.pruebatecnica.robin.academia.dto.CursoJsonDTO;
import autentia.pruebatecnica.robin.academia.ejb.CursoAcademiaService;
import autentia.pruebatecnica.robin.academia.pojo.Curso;
import autentia.pruebatecnica.robin.academia.view.CursoView;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CursoAcademiaRestService.class)
public class TestCursoAcademiaRestService {

	@Autowired
	private MockMvc mockMvc;
	@InjectMocks
	private CursoAcademiaRestService cursoRest;
	@MockBean
	private CursoAcademiaService cursoService;
	@Mock
	private Environment env;
	@Autowired
	private ObjectMapper ObjMapper;
	
	@Test
	public void findActiveById() throws Exception {
		
		try {
			Curso c = new Curso();
			c.setId(1L);
			
			given(cursoService.findActiveById(ArgumentMatchers.anyLong())).willReturn(c);
			
			mockMvc.perform(get("/curso/findById/"+c.getId()).characterEncoding("UTF-8").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.id").value(c.getId()));
		} catch (Throwable e) {
			Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void findViewAllActivePaginatedSorted() throws Exception {
		
		try {
			List<CursoView> result = new ArrayList<CursoView>();
			CursoView c = new CursoView();
			c.setTitulo("curso1");
			result.add(c);
			
			MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
										  queryParams.add("offset", "0");
										  queryParams.add("limit", "1");
										  queryParams.add("sorting", "ASC");
										  
			given(env.getProperty(AcademiaApplication.DEFAULT_CURSO_SORTING_ORDER)).willReturn("ASC");
			given(cursoService.findViewAllActivePaginatedSorted(ArgumentMatchers.any(RowBounds.class), 
																ArgumentMatchers.anyString())).willReturn(result);
			
			mockMvc.perform(get("/curso/findViewAllActivePaginatedSorted").characterEncoding("UTF-8")
			.params(queryParams).contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$[0].titulo").value(result.get(0).getTitulo()));
		} catch (Throwable e) {
			Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void findAllActivePaginatedSorted() throws Exception {
		
		try {
			List<Curso> result = new ArrayList<Curso>();
			Curso c = new Curso();
			c.setTitulo("curso1");
			result.add(c);
			
			MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
										  queryParams.add("offset", "0");
										  queryParams.add("limit", "1");
										  queryParams.add("sorting", "ASC");
										  
			given(env.getProperty(AcademiaApplication.DEFAULT_CURSO_SORTING_ORDER)).willReturn("ASC");
			given(cursoService.findAllActivePaginatedSorted(ArgumentMatchers.any(RowBounds.class), 
																ArgumentMatchers.anyString())).willReturn(result);
			
			mockMvc.perform(get("/curso/findAllCursosActivosPaginated").characterEncoding("UTF-8")
			.params(queryParams).contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$[0].titulo").value(result.get(0).getTitulo()));
		} catch (Throwable e) {
			Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void insertCursoAutoId() throws Exception {
		
		try {
			CursoJsonDTO c = new CursoJsonDTO();
			c.setId(1L);
			c.setNivelId(1L);
			c.setProfesorId(1L);
			c.setActivo(Boolean.TRUE);
			c.setTitulo("titulo1");
			c.setHoras(5L);

			given(cursoService.insertCursoAutoId(ArgumentMatchers.any(Curso.class))).willReturn(c.getId());
			
			mockMvc.perform(MockMvcRequestBuilders.post("/curso/insertCursoAutoId").characterEncoding("UTF-8")
			.content(ObjMapper.writerFor(CursoJsonDTO.class).writeValueAsString(c))
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$").value(c.getId()));
		} catch (Throwable e) {
			Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void insertCursoAutoIdWithFile() throws Exception {
		
		try {
			CursoJsonDTO c = new CursoJsonDTO();
			c.setId(1L);
			c.setNivelId(1L);
			c.setProfesorId(1L);
			c.setActivo(Boolean.TRUE);
			c.setTitulo("titulo1");
			c.setHoras(5L);
			
			MockMultipartFile f = new MockMultipartFile("file", "pdf".getBytes());

			given(cursoService.insertCursoAutoId(ArgumentMatchers.any(Curso.class))).willReturn(c.getId());
			
			mockMvc.perform(MockMvcRequestBuilders.multipart("/curso/insertCursoAutoIdUploadFile")
			.file(f)
			.param("curso", ObjMapper.writerFor(CursoJsonDTO.class).writeValueAsString(c)).characterEncoding("UTF-8"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$").value(c.getId()));
		} catch (Throwable e) {
			Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void updateCurso() throws Exception {
		
		try {
			CursoJsonDTO c = new CursoJsonDTO();
			c.setId(1L);
			c.setNivelId(1L);
			c.setProfesorId(1L);
			c.setActivo(Boolean.TRUE);
			c.setTitulo("titulo1");
			c.setHoras(5L);

			given(cursoService.updateCurso(ArgumentMatchers.any(Curso.class))).willReturn(c.getId());
			
			mockMvc.perform(MockMvcRequestBuilders.put("/curso/updateCurso").characterEncoding("UTF-8")
			.content(ObjMapper.writerFor(CursoJsonDTO.class).writeValueAsString(c))
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$").value(c.getId()));
		} catch (Throwable e) {
			Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void deleteById() throws Exception {
		
		try {
			Long id = 1L;
			
			Long result = 1L;
			
			given(cursoService.deleteById(ArgumentMatchers.anyLong())).willReturn(result);
			
			mockMvc.perform(MockMvcRequestBuilders.delete("/curso/deleteById/"+id)
					.characterEncoding("UTF-8").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$").value(result));
		} catch (Throwable e) {
			Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void downloadFile() throws Exception {
		
		try {
			Curso c = new Curso();
			c.setId(1L);
			c.setTemario("temario".getBytes());
			
			given(cursoService.downloadFile(ArgumentMatchers.anyLong())).willReturn(c.getTemario());
			
			MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/curso/descargarCursoTemario/"+c.getId())
					
					.characterEncoding("UTF-8").contentType(MediaType.APPLICATION_OCTET_STREAM))
			.andExpect(status().isOk())
			.andReturn();
			
			Assertions.assertTrue(Arrays.equals(c.getTemario(), mvcResult.getResponse().getContentAsByteArray()));
		} catch (Throwable e) {
			Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
}
