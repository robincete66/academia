package autentia.pruebatecnica.robin.academia.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.ibatis.session.RowBounds;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import autentia.pruebatecnica.robin.academia.pojo.Curso;

@MybatisTest
@ExtendWith(SpringExtension.class)
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2, replace = Replace.NONE)
public class TestCursoAcademiaMyBatisRepository {

	@MockBean
	private CursoAcademiaMyBatisRepository repo;

	@Test
	public void findActiveById() {

		Long id = 1L;
		
		try {
			Curso c = new Curso();
			c.setId(id);
			
			BDDMockito.given(repo.findActiveById(ArgumentMatchers.anyLong())).willReturn(c);
			
			Curso result = repo.findActiveById(id);
			
			Assertions.assertEquals(c.getId(), result.getId());
			
		} catch (Throwable e) {
			Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
			Assertions.fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void findAllActivePaginatedSorted() {

		Long id = 1L;
		
		try {
			List<Curso> expected = new ArrayList<Curso>();
			Curso c = new Curso();
			c.setId(id);
			
			expected.add(c);
			
			BDDMockito.given(repo.findAllActivePaginatedSorted(ArgumentMatchers.any(RowBounds.class), ArgumentMatchers.anyString())).willReturn(expected);
			
			List<Curso> actual = repo.findAllActivePaginatedSorted(new RowBounds(0, 1), "ASC");
			
			Assertions.assertTrue(expected.equals(actual));
			
		} catch (Throwable e) {
			Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
			Assertions.fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void deleteById() {

		Long expected = 1L;
		
		try {
			
			BDDMockito.given(repo.deleteById(ArgumentMatchers.anyLong())).willReturn(expected);
			
			Long actual = repo.deleteById(expected);
			
			Assertions.assertEquals(expected, actual);
			
		} catch (Throwable e) {
			Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
			Assertions.fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void insertCursoAutoId() {

		String expected = "titulo1";
		
		try {
			Curso c = new Curso();
			c.setTitulo(expected);
			
			BDDMockito.given(repo.insertCursoAutoId(ArgumentMatchers.any(Curso.class))).willReturn(ArgumentMatchers.anyLong());
			
			repo.insertCursoAutoId(c);
			
			BDDMockito.verify(repo).insertCursoAutoId(c);
			
		} catch (Throwable e) {
			Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
			Assertions.fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void insertCursoManualId() {

		Long expected = 1L;
		
		try {
			Curso c = new Curso();
			c.setId(expected);
			
			BDDMockito.given(repo.insertCursoManualId(ArgumentMatchers.any(Curso.class))).willReturn(expected);
			
			Long actual = repo.insertCursoManualId(c);
			
			Assertions.assertEquals(c.getId(), actual);
			
		} catch (Throwable e) {
			Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
			Assertions.fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void updateCurso() {

		String expected = "titulo1";
		
		try {
			Curso c = new Curso();
			c.setTitulo(expected);
			
			BDDMockito.given(repo.updateCurso(ArgumentMatchers.any(Curso.class))).willReturn(ArgumentMatchers.anyLong());
			
			repo.updateCurso(c);
			
			BDDMockito.verify(repo).updateCurso(c);
			
		} catch (Throwable e) {
			Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
			Assertions.fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void downloadFile() {

		Long id = 1L;
		
		try {
			Curso c = new Curso();
			c.setId(id);
			
			BDDMockito.given(repo.downloadFile(ArgumentMatchers.anyLong())).willReturn(c);
			
			Curso result = repo.downloadFile(id);
			
			Assertions.assertEquals(c.getId(), result.getId());
			
		} catch (Throwable e) {
			Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
			Assertions.fail(e.getMessage(), e);
		}
	}

}
