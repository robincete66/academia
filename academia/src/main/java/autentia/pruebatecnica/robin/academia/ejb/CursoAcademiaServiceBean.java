package autentia.pruebatecnica.robin.academia.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import autentia.pruebatecnica.robin.academia.AcademiaApplication;
import autentia.pruebatecnica.robin.academia.dao.CursoAcademiaDAO;
import autentia.pruebatecnica.robin.academia.pojo.Curso;
import autentia.pruebatecnica.robin.academia.view.CursoView;

/**
 * 
 * @author robin
 * @since 0.0.1.Final
 *
 */
@Stateless(name = "CursoAcademia")
@Service
public class CursoAcademiaServiceBean implements CursoAcademiaService {

	private Logger log;
	@Autowired
	private CursoAcademiaDAO cursoDAO; 

	public CursoAcademiaServiceBean() {

	}
	
	@Override
	public RowBounds validateRowBoundsValues(Integer offset, Integer limit) {
		return (offset!=null && limit!=null && offset.compareTo(0) > -1 && offset.compareTo(limit) < 1) ? new RowBounds(offset, limit) : null;
	}
	
	@Override
	public String validateSortingValue(String sorting) {
		return sorting!=null && ("DESC".equalsIgnoreCase(sorting.trim()) || "ASC".equalsIgnoreCase(sorting.trim())) ? sorting.trim().toUpperCase() : null;
	}

	@Override
	public Curso findActiveById(Long id) {
		return cursoDAO.findActiveById(id);
	}

	@Override
	public Long deleteById(Long id) {
		return cursoDAO.deleteById(id);
	}

	@Override
	public Long insertCursoAutoId(Curso curso) {
		return cursoDAO.insertCursoAutoId(curso);
	}
	
	@Override
	public Long insertCursoManualId(Curso curso) {
		return cursoDAO.insertCursoManualId(curso);
	}

	@Override
	public Long updateCurso(Curso curso) {
		return cursoDAO.updateCurso(curso);
	}

	@Override
	public List<Curso> findAllActivePaginatedSorted(RowBounds pagination, String sorting) {
		return cursoDAO.findAllActivePaginatedSorted(pagination, sorting);
	}

	public Logger getLog() {
		return log;
	}

	@Qualifier(value = AcademiaApplication.BEAN_CURSO_ACADEMIA_LOGGER)
	@Autowired
	public void setLog(Logger log) {
		this.log = log;
	}

	public CursoAcademiaDAO getCursoDAO() {
		return cursoDAO;
	}

	public void setCursoDAO(CursoAcademiaDAO cursoDAO) {
		this.cursoDAO = cursoDAO;
	}

	@Override
	public byte[] downloadFile(Long id) {
		return this.cursoDAO.downloadFile(id);
	}

	@Override
	public List<CursoView> findViewAllActivePaginatedSorted(RowBounds pagination, String sorting) {
		return this.cursoDAO.findViewAllActivePaginatedSorted(pagination, sorting);
	}

	

}
