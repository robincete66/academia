package autentia.pruebatecnica.robin.academia.http;

import autentia.pruebatecnica.robin.academia.dto.CursoJsonDTO;

public class FicheroHttpRequest {

	private CursoJsonDTO curso;
	private FicheroHttp fichero;

	public FicheroHttpRequest(CursoJsonDTO curso, FicheroHttp fichero) {
		super();
		this.curso = curso;
		this.fichero = fichero;
	}

	public CursoJsonDTO getCurso() {
		return curso;
	}

	public FicheroHttp getFichero() {
		return fichero;
	}

}
