package autentia.pruebatecnica.robin.academia.http.handler;

import autentia.pruebatecnica.robin.academia.http.FicheroHttpRequest;

@FunctionalInterface
public interface FicheroHttpRequestHandler<T> {

	public T handle(FicheroHttpRequest request);
}
