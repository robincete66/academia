package autentia.pruebatecnica.robin.academia.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import autentia.pruebatecnica.robin.academia.pojo.Curso;
import autentia.pruebatecnica.robin.academia.repository.CursoAcademiaMyBatisRepository;
import autentia.pruebatecnica.robin.academia.view.CursoView;

@Repository
public class CursoAcademiaDAOImpl implements CursoAcademiaDAO {

	@Autowired
	private CursoAcademiaMyBatisRepository cursoRepo;
	
	public CursoAcademiaDAOImpl() {
		
	}

	@Override
	public Curso findActiveById(Long id) {
		return cursoRepo.findActiveById(id);
	}

	@Override
	public Long deleteById(Long id) {
		return cursoRepo.deleteById(id);
	}

	@Override
	public Long insertCursoAutoId(Curso curso) {
		return cursoRepo.insertCursoAutoId(curso);
	}
	
	@Override
	public Long insertCursoManualId(Curso curso) {
		return cursoRepo.insertCursoManualId(curso);
	}

	@Override
	public Long updateCurso(Curso curso) {
		return cursoRepo.updateCurso(curso);
	}

	@Override
	public List<Curso> findAllActivePaginatedSorted(RowBounds pagination, String sorting) {
		return cursoRepo.findAllActivePaginatedSorted(pagination, sorting);
	}

	public CursoAcademiaMyBatisRepository getCursoRepo() {
		return cursoRepo;
	}

	public void setCursoRepo(CursoAcademiaMyBatisRepository cursoRepo) {
		this.cursoRepo = cursoRepo;
	}

	@Override
	public byte[] downloadFile(Long id) {
		Curso file = this.cursoRepo.downloadFile(id);
		if (file!=null) {
			return file.getTemario();
		} else {
			return null;
		}
	}

	@Override
	public List<CursoView> findViewAllActivePaginatedSorted(RowBounds pagination, String sorting) {
		List<Curso> c = this.cursoRepo.findAllActivePaginatedSorted(pagination, sorting);
		if (c!=null && !c.isEmpty()) {
			List<CursoView> v = new ArrayList<CursoView>();
			c.forEach(e -> v.add(new CursoView(e.getProfesor().getNombre()+" "+e.getProfesor().getApellidos(), e.getTitulo(), e.getHoras(), e.getNivel().getDescripcion(), e.getId())));
			return v;
		} else {
			return null;
		}
	}
	
	
	
	
}
