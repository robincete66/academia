package autentia.pruebatecnica.robin.academia.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import autentia.pruebatecnica.robin.academia.AcademiaApplication;
import autentia.pruebatecnica.robin.academia.dao.ProfesorAcademiaDAO;
import autentia.pruebatecnica.robin.academia.pojo.Profesor;

/**
 * 
 * @author robin
 * @since 0.0.1.Final
 *
 */
@Stateless(name = "ProfesorAcademia")
@Service
public class ProfesorAcademiaServiceBean implements ProfesorAcademiaService {

	private Logger log;
	@Autowired
	private ProfesorAcademiaDAO profesorDAO;

	public ProfesorAcademiaServiceBean() {

	}

	@Override
	public List<Profesor> findAll() {
		return profesorDAO.findAll();
	}

	@Override
	public Profesor findById(Long id) {
		return profesorDAO.findById(id);
	}

	@Override
	public Long deleteById(Long id) {
		return profesorDAO.deleteById(id);
	}

	@Override
	public Long insert(Profesor profesor) {
		return profesorDAO.insert(profesor);
	}

	@Override
	public Long update(Profesor profesor) {
		return profesorDAO.update(profesor);
	}

	public Logger getLog() {
		return log;
	}

	@Qualifier(value = AcademiaApplication.BEAN_PROFESOR_ACADEMIA_LOGGER)
	@Autowired
	public void setLog(Logger log) {
		this.log = log;
	}

	public ProfesorAcademiaDAO getProfesorDAO() {
		return profesorDAO;
	}

	public void setProfesorDAO(ProfesorAcademiaDAO profesorDAO) {
		this.profesorDAO = profesorDAO;
	}
	

}
