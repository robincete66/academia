package autentia.pruebatecnica.robin.academia.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProfesorJsonDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("id")
	private Long id;
	@JsonProperty("nombre")
	private String nombre;
	@JsonProperty("apellidos")
	private String apellidos;
	@JsonProperty("cursos")
	private Set<CursoJsonDTO> cursos;

	public ProfesorJsonDTO() {

	}

	public ProfesorJsonDTO(String nombre, String apellidos) {
		super();
		this.nombre = nombre;
		this.apellidos = apellidos;
	}

	public ProfesorJsonDTO(Long id, String nombre, String apellidos) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellidos = apellidos;
	}

	public ProfesorJsonDTO(String nombre, String apellidos, Set<CursoJsonDTO> cursos) {
		super();
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.cursos = cursos;
	}

	public ProfesorJsonDTO(Long id, String nombre, String apellidos, Set<CursoJsonDTO> cursos) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.cursos = cursos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Set<CursoJsonDTO> getCursos() {
		return cursos;
	}

	public void setCursos(Set<CursoJsonDTO> cursos) {
		this.cursos = cursos;
	}

	@Override
	public int hashCode() {
		return Objects.hash(apellidos, id, nombre);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ProfesorJsonDTO)) {
			return false;
		}
		ProfesorJsonDTO other = (ProfesorJsonDTO) obj;
		return Objects.equals(apellidos, other.apellidos)
				&& Objects.equals(id, other.id) && Objects.equals(nombre, other.nombre);
	}

}
