package autentia.pruebatecnica.robin.academia.pojo;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Entity;

import org.apache.ibatis.type.Alias;

@Alias("curso")
@Entity
public class Curso implements Serializable, Comparable<Curso> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Boolean activo;
	private Profesor profesor;
	private String titulo;
	private Long horas;
	private Nivel nivel;
	private byte[] temario;

	public Curso() {

	}

	public Curso(Long id, Boolean activo, Profesor profesor, String titulo, Long horas, Nivel nivel) {
		super();
		this.id = id;
		this.activo = activo;
		this.profesor = profesor;
		this.titulo = titulo;
		this.horas = horas;
		this.nivel = nivel;
	}

	public Curso(Boolean activo, Profesor profesor, String titulo, Long horas, Nivel nivel) {
		super();
		this.activo = activo;
		this.profesor = profesor;
		this.titulo = titulo;
		this.horas = horas;
		this.nivel = nivel;
	}

	public Curso(Long id, Boolean activo, Profesor profesor, String titulo, Long horas, Nivel nivel,
			byte[] temario) {
		super();
		this.id = id;
		this.activo = activo;
		this.profesor = profesor;
		this.titulo = titulo;
		this.horas = horas;
		this.nivel = nivel;
		this.temario = temario;
	}

	public Curso(Boolean activo, Profesor profesor, String titulo, Long horas, Nivel nivel, byte[] temario) {
		super();
		this.activo = activo;
		this.profesor = profesor;
		this.titulo = titulo;
		this.horas = horas;
		this.nivel = nivel;
		this.temario = temario;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Profesor getProfesor() {
		return profesor;
	}

	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Long getHoras() {
		return horas;
	}

	public void setHoras(Long horas) {
		this.horas = horas;
	}

	public Nivel getNivel() {
		return nivel;
	}

	public void setNivel(Nivel nivel) {
		this.nivel = nivel;
	}

	public byte[] getTemario() {
		return temario;
	}

	public void setTemario(byte[] temario) {
		this.temario = temario;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(activo, id, nivel, horas, profesor, titulo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Curso)) {
			return false;
		}
		Curso other = (Curso) obj;
		return Objects.equals(activo, other.activo) && Objects.equals(id, other.id)
				&& Objects.equals(nivel, other.nivel) && Objects.equals(horas, other.horas)
				&& Objects.equals(profesor, other.profesor)	&& Objects.equals(titulo, other.titulo);
	}

	@Override
	public String toString() {
		return "Curso [id=" + id + ", activo=" + activo + ", profesor=" + profesor + ", titulo=" + titulo
				+ ", horas=" + horas + ", nivel=" + nivel + ", temario=" + temario + "]";
	}

	@Override
	public int compareTo(Curso o) {
		return this.getTitulo().compareToIgnoreCase(o.getTitulo());
	}

}
