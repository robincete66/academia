package autentia.pruebatecnica.robin.academia.comparator;

import java.util.Comparator;

import autentia.pruebatecnica.robin.academia.pojo.Curso;

public class CursoAcademiaComparator implements Comparator<Curso> {

	@Override
	public int compare(Curso o1, Curso o2) {
		return o1.compareTo(o2);
	}

}
