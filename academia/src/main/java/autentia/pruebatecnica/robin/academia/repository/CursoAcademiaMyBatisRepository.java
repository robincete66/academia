package autentia.pruebatecnica.robin.academia.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;

import autentia.pruebatecnica.robin.academia.pojo.Curso;

@Mapper
@Repository
public interface CursoAcademiaMyBatisRepository {
	
	@ResultMap("QueryCursoMap")
	@ResultType(Curso.class)
	public List<Curso> findAllActivePaginatedSorted(RowBounds pagination, @Param("sorting") String sorting);

	@ResultMap("QueryCursoMap")
	@ResultType(Curso.class)
	public Curso findActiveById(@Param("idParam") Long id);

	public Long deleteById(@Param("id") Long id);

	public Long insertCursoAutoId(@Param("curso") Curso curso);
	
	public Long insertCursoManualId(@Param("curso") Curso curso);

	public Long updateCurso(@Param("curso") Curso curso);

	
	@ResultMap("downloadFileMap")
	@ResultType(Curso.class)
	public Curso downloadFile(@Param("id") Long id);

}
