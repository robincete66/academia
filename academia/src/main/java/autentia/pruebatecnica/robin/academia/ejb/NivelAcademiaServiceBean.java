package autentia.pruebatecnica.robin.academia.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import autentia.pruebatecnica.robin.academia.AcademiaApplication;
import autentia.pruebatecnica.robin.academia.dao.NivelAcademiaDAO;
import autentia.pruebatecnica.robin.academia.pojo.Nivel;

@Stateless(name = "NivelAcademiaService")
@Service
public class NivelAcademiaServiceBean implements NivelAcademiaService {

	private Logger log;
	@Autowired
	private NivelAcademiaDAO nivelDao;
	
	@Override
	public Nivel findById(Long id) {
		return nivelDao.findById(id);
	}

	@Override
	public List<Nivel> findAll() {
		return nivelDao.findAll();
	}
	
	public Logger getLog() {
		return log;
	}

	@Qualifier(value = AcademiaApplication.BEAN_NIVEL_ACADEMIA_LOGGER)
	@Autowired
	public void setLog(Logger log) {
		this.log = log;
	}

	
}
