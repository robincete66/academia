package autentia.pruebatecnica.robin.academia.dao;

import java.util.List;

import org.apache.ibatis.session.RowBounds;

import autentia.pruebatecnica.robin.academia.pojo.Curso;
import autentia.pruebatecnica.robin.academia.view.CursoView;

public interface CursoAcademiaDAO {

	public List<Curso> findAllActivePaginatedSorted(RowBounds pagination, String sorting);
	public List<CursoView> findViewAllActivePaginatedSorted(RowBounds pagination, String sorting);
	public Curso findActiveById(Long id);
	public Long deleteById(Long id);
	public Long updateCurso(Curso curso);
	public Long insertCursoAutoId(Curso curso);
	public Long insertCursoManualId(Curso curso);
	public byte[] downloadFile(Long id);
	
}
