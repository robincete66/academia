package autentia.pruebatecnica.robin.academia.dtoParsers;

import autentia.pruebatecnica.robin.academia.dto.NivelJsonDTO;
import autentia.pruebatecnica.robin.academia.pojo.Nivel;

public class NivelJsonDtoParser implements DtoParser<Nivel, NivelJsonDTO>{

	@Override
	public Nivel parseDto(NivelJsonDTO dto) {
		return new Nivel(dto.getId(), dto.getDescripcion());
	}

}
