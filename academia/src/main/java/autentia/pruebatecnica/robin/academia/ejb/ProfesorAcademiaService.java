package autentia.pruebatecnica.robin.academia.ejb;

import java.util.List;

import javax.ejb.Local;

import autentia.pruebatecnica.robin.academia.pojo.Profesor;

/**
 * 
 * @author robin
 * @since 0.0.1.Final
 *
 */
@Local
public interface ProfesorAcademiaService {

	public List<Profesor> findAll();
	public Profesor findById(Long id);
	public Long deleteById(Long id);
	public Long insert(Profesor curso);
	public Long update(Profesor curso);
	
}
