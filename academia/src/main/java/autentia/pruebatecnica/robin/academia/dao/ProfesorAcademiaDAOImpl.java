package autentia.pruebatecnica.robin.academia.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import autentia.pruebatecnica.robin.academia.pojo.Profesor;
import autentia.pruebatecnica.robin.academia.repository.ProfesorAcademiaMyBatisRepository;

@Repository
public class ProfesorAcademiaDAOImpl implements ProfesorAcademiaDAO {
	
	@Autowired
	private ProfesorAcademiaMyBatisRepository profesorRepo;

	@Override
	public List<Profesor> findAll() {
		return profesorRepo.findAll();
	}

	@Override
	public Profesor findById(Long id) {
		return profesorRepo.findById(id);
	}

	@Override
	public Long deleteById(Long id) {
		return profesorRepo.deleteById(id);
	}

	@Override
	public Long insert(Profesor profesor) {
		return profesorRepo.insert(profesor);
	}

	@Override
	public Long update(Profesor profesor) {
		return profesorRepo.update(profesor);
	}

	public ProfesorAcademiaMyBatisRepository getProfesorRepo() {
		return profesorRepo;
	}

	public void setProfesorRepo(ProfesorAcademiaMyBatisRepository profesorRepo) {
		this.profesorRepo = profesorRepo;
	}
	
	

}
