package autentia.pruebatecnica.robin.academia.dto;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NivelJsonDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("id")
	private Long id;
	@JsonProperty("descripcion")
	private String descripcion;
	
	public NivelJsonDTO() {
		
	}

	public NivelJsonDTO(Long id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "NivelJsonDTO [id=" + id + ", descripcion=" + descripcion + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(descripcion, id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof NivelJsonDTO)) {
			return false;
		}
		NivelJsonDTO other = (NivelJsonDTO) obj;
		return Objects.equals(descripcion, other.descripcion) && Objects.equals(id, other.id);
	}
	
}
