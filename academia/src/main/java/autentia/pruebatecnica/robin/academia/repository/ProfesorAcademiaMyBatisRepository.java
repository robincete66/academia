package autentia.pruebatecnica.robin.academia.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import autentia.pruebatecnica.robin.academia.pojo.Profesor;

@Mapper
@Repository
public interface ProfesorAcademiaMyBatisRepository {

	@ResultMap("queryProfesoresMap")
	@ResultType(Profesor.class)
	public List<Profesor> findAll();

	@ResultMap("queryProfesoresMap")
	@ResultType(Profesor.class)
	public Profesor findById(@Param("idParam") Long id);

	@Delete("DELETE FROM profesor WHERE id = #{idParam}")
	public Long deleteById(@Param("idParam") Long id);

	@Insert("INSERT INTO profesor (nombre, apellidos) "
			+ " VALUES (#{profesor.nombre}, #{profesor.apellidos})")
	@Options(keyColumn = "id", keyProperty = "id", useGeneratedKeys = true)
	public Long insert(@Param("profesor") Profesor profesor);

	@Update("Update profesor set nombre=#{profesor.nombre}, "
			+ " apellidos=#{profesor.apellidos} where id=#{profesor.id}")
	public Long update(@Param("profesor") Profesor profesor);
	
}
