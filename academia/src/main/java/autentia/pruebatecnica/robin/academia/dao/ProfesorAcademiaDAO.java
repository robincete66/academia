package autentia.pruebatecnica.robin.academia.dao;

import java.util.List;

import autentia.pruebatecnica.robin.academia.pojo.Profesor;

public interface ProfesorAcademiaDAO {

	public List<Profesor> findAll();
	public Profesor findById(Long id);
	public Long deleteById(Long id);
	public Long insert(Profesor curso);
	public Long update(Profesor curso);
	
}
