package autentia.pruebatecnica.robin.academia.view;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class CursoView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String profesor;
	private String titulo;
	private Long horas;
	private String nivel;
	private Long temario;

	public CursoView() {
		
	}

	public CursoView(String profesor, String titulo, Long horas, String nivel, Long temario) {
		super();
		this.profesor = profesor;
		this.titulo = titulo;
		this.horas = horas;
		this.nivel = nivel;
		this.temario=temario;
	}

	public String getProfesor() {
		return profesor;
	}

	public void setProfesor(String profesor) {
		this.profesor = profesor;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Long getHoras() {
		return horas;
	}

	public void setHoras(Long horas) {
		this.horas = horas;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	@Override
	public String toString() {
		return "CursoView [profesor=" + profesor + ", titulo=" + titulo + ", horas=" + horas + ", nivel=" + nivel + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(horas, nivel, profesor, titulo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof CursoView)) {
			return false;
		}
		CursoView other = (CursoView) obj;
		return Objects.equals(horas, other.horas) && Objects.equals(nivel, other.nivel)
				&& Objects.equals(profesor, other.profesor) && Objects.equals(titulo, other.titulo);
	}

	public Long getTemario() {
		return temario;
	}

	public void setTemario(Long temario) {
		this.temario = temario;
	}

}
