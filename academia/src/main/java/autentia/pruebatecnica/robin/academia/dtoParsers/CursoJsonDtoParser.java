package autentia.pruebatecnica.robin.academia.dtoParsers;

import org.springframework.stereotype.Component;

import autentia.pruebatecnica.robin.academia.dto.CursoJsonDTO;
import autentia.pruebatecnica.robin.academia.pojo.Curso;
import autentia.pruebatecnica.robin.academia.pojo.Nivel;
import autentia.pruebatecnica.robin.academia.pojo.Profesor;

@Component
public class CursoJsonDtoParser implements DtoParser<Curso, CursoJsonDTO> {

	@Override
	public Curso parseDto(CursoJsonDTO dto) {
		if (dto==null) {
			return null;
		}
		Curso c = new Curso();
		c.setId(dto.getId());
		c.setActivo(dto.getActivo());
		c.setHoras(dto.getHoras());
		Nivel n = new Nivel();
		n.setId(dto.getNivelId());
		c.setNivel(n);
		Profesor p = new Profesor();
		p.setId(dto.getProfesorId());
		c.setProfesor(p);
		c.setTitulo(dto.getTitulo());
		
		return c;
	}
}
