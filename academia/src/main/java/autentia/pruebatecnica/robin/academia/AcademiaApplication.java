package autentia.pruebatecnica.robin.academia;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;
import javax.ws.rs.ApplicationPath;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.transaction.managed.ManagedTransactionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.atomikos.icatch.jta.UserTransactionImp;
import com.atomikos.icatch.jta.UserTransactionManager;

import autentia.pruebatecnica.robin.academia.dtoParsers.ProfesorJsonDtoParser;

/**
 * 
 * @author robin
 * @since 0.0.1.Final
 *
 */
@SpringBootApplication
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class, MybatisAutoConfiguration.class })
@MapperScan("autentia.pruebatecnica.robin.academia.repository")
@PropertySource("classpath:application.properties")
@ComponentScan
@ApplicationPath("/academia")
public class AcademiaApplication implements EnvironmentAware {

	public static final String BEAN_CURSO_ACADEMIA_LOGGER = "cursoAcademiaLoggerBean";
	public static final String BEAN_PROFESOR_ACADEMIA_LOGGER = "profesorAcademiaLoggerBean";
	public static final String BEAN_NIVEL_ACADEMIA_LOGGER = "nivelAcademiaLoggerBean";
	public static final String BEAN_INITIAL_CONTEXT = "initialContextBean";
	public static final String DEFAULT_CURSO_SORTING_ORDER = "sql.default.curso-sorting";

	private static Environment env;

	@Autowired
	private Context initialContext;

	public static void main(String[] args) {
		SpringApplication.run(AcademiaApplication.class, args);
	}
	
	/*@Bean
	public CursoJsonDtoParser cursoJsonDtoParser() {
		return new CursoJsonDtoParser();
	}*/
	
	@Bean
	public ProfesorJsonDtoParser profesorJsonDtoParser() {
		return new ProfesorJsonDtoParser();
	}

	@Bean
	public DataSource dataSource() {
		return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2)
					.addScript("curso.sql").addScript("inserts.sql").build();
	}
	
	@Bean
	public UserTransactionManager userTransactionManager() {
		return new UserTransactionManager();
	}
	
	@Bean
	public UserTransaction userTransaction() {
		return new UserTransactionImp();
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public SqlSessionFactory sqlSessionFactory() throws Exception {
		SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
		factoryBean.setDataSource(dataSource());
		factoryBean.setTypeHandlersPackage("autentia.pruebatecnica.robin.academia.typeHandlers");
		factoryBean.setTransactionFactory(new ManagedTransactionFactory());
		return factoryBean.getObject();
	}

	@Bean(name = AcademiaApplication.BEAN_INITIAL_CONTEXT)
	public Context getContext() throws NamingException {
		return new InitialContext();
	}
	
	@Bean
	public CommonsMultipartResolver commonsMultipartResolver() {
		//org.glassfish.jersey.media.multipart.FormDataContentDisposition d;
		return new CommonsMultipartResolver();
	}

	@Bean(name = BEAN_CURSO_ACADEMIA_LOGGER)
	@Value("${logger.academia.curso.logger-name:CursoAcademiaServiceLogger}")
	public Logger getCursoAcademiaServiceLogger() {
		return LoggerFactory.getILoggerFactory().getLogger(env.getProperty("logger.academia.curso.logger-name"));
	}
	
	@Bean(name = BEAN_NIVEL_ACADEMIA_LOGGER)
	@Value("${logger.academia.nivel.logger-name:NivelAcademiaServiceLogger}")
	public Logger getNivelAcademiaServiceLogger() {
		return LoggerFactory.getILoggerFactory().getLogger(env.getProperty("logger.academia.nivel.logger-name"));
	}

	@Bean(name = BEAN_PROFESOR_ACADEMIA_LOGGER)
	@Value("${logger.academia.profesor.logger-name:ProfesorAcademiaServiceLogger}")
	public Logger getProfesorAcademiaServiceLogger() {
		return LoggerFactory.getILoggerFactory().getLogger(env.getProperty("logger.academia.profesor.logger-name"));
	}

	@Override
	public void setEnvironment(Environment environment) {
		AcademiaApplication.setEnv(environment);
	}

	public static Environment getEnv() {
		return AcademiaApplication.env;
	}

	public static void setEnv(Environment env) {
		AcademiaApplication.env = env;
	}

	public void setInitialContext(Context initialContext) {
		this.initialContext = initialContext;
	}

	public Context getInitialContext() {
		return this.initialContext;
	}
}
