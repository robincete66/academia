package autentia.pruebatecnica.robin.academia.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import autentia.pruebatecnica.robin.academia.pojo.Nivel;

@Mapper
@Repository
public interface NivelAcademiaMyBatisRepository {

	@Select("SELECT * FROM nivel WHERE id = #{id}")
	@Results(value = {
			@Result(column = "id", id = true, property = "id"),
			@Result(column = "descripcion", property = "descripcion")
	})
	public Nivel findById(@Param("id") Long id);
	
	@Select("SELECT * FROM nivel")
	@Results(value = {
			@Result(column = "id", id = true, property = "id"),
			@Result(column = "descripcion", property = "descripcion")
	})
	public List<Nivel> findAll();
}
