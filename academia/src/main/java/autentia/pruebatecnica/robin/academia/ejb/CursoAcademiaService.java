package autentia.pruebatecnica.robin.academia.ejb;

import java.util.List;

import javax.ejb.Local;

import org.apache.ibatis.session.RowBounds;

import autentia.pruebatecnica.robin.academia.pojo.Curso;
import autentia.pruebatecnica.robin.academia.view.CursoView;

/**
 * 
 * @author robin
 * @since 0.0.1.Final
 *
 */
@Local
public interface CursoAcademiaService {
	
	public List<Curso> findAllActivePaginatedSorted(RowBounds pagination, String sorting);
	public List<CursoView> findViewAllActivePaginatedSorted(RowBounds pagination, String sorting);
	public Curso findActiveById(Long id);
	public Long deleteById(Long id);
	public RowBounds validateRowBoundsValues(Integer offset, Integer limit);
	public String validateSortingValue(String sorting);
	public Long insertCursoAutoId(Curso curso);
	public Long updateCurso(Curso curso);
	public Long insertCursoManualId(Curso curso);
	public byte[] downloadFile(Long id);

}
