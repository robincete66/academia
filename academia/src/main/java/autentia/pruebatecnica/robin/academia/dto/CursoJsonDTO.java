package autentia.pruebatecnica.robin.academia.dto;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class CursoJsonDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	private Long id;
	@JsonProperty("activo")
	private Boolean activo;
	@JsonProperty("profesor")
	private Long profesorId;
	@JsonProperty("titulo")
	private String titulo;
	@JsonProperty("horas")
	private Long horas;
	@JsonProperty("nivel")
	private Long nivelId;

	public CursoJsonDTO() {

	}

	public CursoJsonDTO(Boolean activo, Long profesorId, String titulo, Long horas, Long nivelId) {
		super();
		this.activo = activo;
		this.profesorId = profesorId;
		this.titulo = titulo;
		this.horas = horas;
		this.nivelId = nivelId;
	}

	public CursoJsonDTO(Long id, Boolean activo, Long profesorId, String titulo, Long horas, Long nivelId) {
		super();
		this.id = id;
		this.activo = activo;
		this.profesorId = profesorId;
		this.titulo = titulo;
		this.horas = horas;
		this.nivelId = nivelId;
	}
	
	@JsonIgnore
	public Long getId() {
		return id;
	}

	@JsonIgnore
	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Long getProfesorId() {
		return profesorId;
	}

	public void setProfesorId(Long profesorId) {
		this.profesorId = profesorId;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Long getHoras() {
		return horas;
	}

	public void setHoras(Long horas) {
		this.horas = horas;
	}

	public Long getNivelId() {
		return nivelId;
	}

	public void setNivelId(Long nivelId) {
		this.nivelId = nivelId;
	}

	@Override
	public String toString() {
		return "CursoJsonDTO [id=" + id + ", activo=" + activo + ", profesorId=" + profesorId + ", titulo=" + titulo
				+ ", horas=" + horas + ", nivelId=" + nivelId + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(activo, horas, id, nivelId, profesorId, titulo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof CursoJsonDTO)) {
			return false;
		}
		CursoJsonDTO other = (CursoJsonDTO) obj;
		return Objects.equals(activo, other.activo) && Objects.equals(horas, other.horas)
				&& Objects.equals(id, other.id) && Objects.equals(nivelId, other.nivelId)
				&& Objects.equals(profesorId, other.profesorId) && Objects.equals(titulo, other.titulo);
	}

}
