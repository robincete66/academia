package autentia.pruebatecnica.robin.academia.dtoParsers;

@FunctionalInterface
public interface DtoParser<T, N> {

	public T parseDto(N dto);
}
