package autentia.pruebatecnica.robin.academia.http;

import java.io.InputStream;

public class FicheroHttp {

	private String nombre;
	private String nombreFichero;
	private Long length;
	private InputStream fichero;

	public FicheroHttp(String nombre, String nombreFichero, Long length,
			InputStream fichero) {
		super();
		this.nombre = nombre;
		this.nombreFichero = nombreFichero;
		this.length = length;
		this.fichero = fichero;
	}
	
	public FicheroHttp(InputStream fichero) {
		super();
		this.fichero = fichero;
	}

	public String getNombre() {
		return nombre;
	}

	public String getNombreFichero() {
		return nombreFichero;
	}

	public Long getLength() {
		return length;
	}

	public InputStream getFichero() {
		return fichero;
	}

}
