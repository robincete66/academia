package autentia.pruebatecnica.robin.academia.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import autentia.pruebatecnica.robin.academia.dto.ProfesorJsonDTO;
import autentia.pruebatecnica.robin.academia.dtoParsers.ProfesorJsonDtoParser;
import autentia.pruebatecnica.robin.academia.ejb.ProfesorAcademiaService;
import autentia.pruebatecnica.robin.academia.pojo.Profesor;

@RestController
@RequestMapping("/profesor")
public class ProfesorAcademiaRestService {

	@Autowired
	private ProfesorAcademiaService profesorService;
	@Autowired
	private ProfesorJsonDtoParser profesorJsonParser;

	@GetMapping("/findAll")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Profesor> findAll() {
		return this.profesorService.findAll();
	}

	@GetMapping("/findById/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Profesor findById(@PathVariable("id") Long id) {
		return this.profesorService.findById(id);
	}

	@DeleteMapping("/deleteById/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Long deleteById(@PathVariable("id") Long id) {
		return this.profesorService.deleteById(id);
	}

	@PostMapping("/insertProfesorAutoId")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Long insert(@RequestBody ProfesorJsonDTO profesor) {
		return this.profesorService.insert(this.profesorJsonParser.parseDto(profesor));
	}

	@PutMapping("/updateProfesor")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Long update(@RequestBody ProfesorJsonDTO profesor) {
		return this.profesorService.update(this.profesorJsonParser.parseDto(profesor));
	}

}
