package autentia.pruebatecnica.robin.academia.dao;

import java.util.List;

import autentia.pruebatecnica.robin.academia.pojo.Nivel;

public interface NivelAcademiaDAO {
	
	public Nivel findById(Long id);
	public List<Nivel> findAll();
	

}
