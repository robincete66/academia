package autentia.pruebatecnica.robin.academia.ejb;

import java.util.List;

import javax.ejb.Local;

import autentia.pruebatecnica.robin.academia.pojo.Nivel;

@Local
public interface NivelAcademiaService {

	public Nivel findById(Long id);
	public List<Nivel> findAll();
}
