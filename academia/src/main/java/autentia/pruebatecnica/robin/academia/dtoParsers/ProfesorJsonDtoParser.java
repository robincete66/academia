package autentia.pruebatecnica.robin.academia.dtoParsers;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;

import autentia.pruebatecnica.robin.academia.dto.CursoJsonDTO;
import autentia.pruebatecnica.robin.academia.dto.ProfesorJsonDTO;
import autentia.pruebatecnica.robin.academia.pojo.Profesor;

public class  ProfesorJsonDtoParser implements DtoParser<Profesor, ProfesorJsonDTO> {

	@Autowired
	private CursoJsonDtoParser cursoJsonParser;
	
	@Override
	public Profesor parseDto(ProfesorJsonDTO dto) {
		
		Profesor p = new Profesor(dto.getId(), dto.getNombre(), 
				dto.getApellidos());
		
		if (dto.getCursos()!=null && !dto.getCursos().isEmpty()) {
			try (Stream<CursoJsonDTO> stream = dto.getCursos().stream()) {
				p.setCursos(stream.map(json -> cursoJsonParser.parseDto(json)).collect(Collectors.toSet()));
			} 
		}
		
		return p;
	}

}
