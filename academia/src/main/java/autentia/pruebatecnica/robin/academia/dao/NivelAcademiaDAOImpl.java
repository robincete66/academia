package autentia.pruebatecnica.robin.academia.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import autentia.pruebatecnica.robin.academia.pojo.Nivel;
import autentia.pruebatecnica.robin.academia.repository.NivelAcademiaMyBatisRepository;

@Repository
public class NivelAcademiaDAOImpl implements NivelAcademiaDAO {

	@Autowired
	private NivelAcademiaMyBatisRepository nivelRepo;
	
	@Override
	public Nivel findById(Long id) {
		return nivelRepo.findById(id);
	}

	@Override
	public List<Nivel> findAll() {
		return nivelRepo.findAll();
	}

}
