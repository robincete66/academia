package autentia.pruebatecnica.robin.academia.rest;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import autentia.pruebatecnica.robin.academia.AcademiaApplication;
import autentia.pruebatecnica.robin.academia.dto.CursoJsonDTO;
import autentia.pruebatecnica.robin.academia.dtoParsers.CursoJsonDtoParser;
import autentia.pruebatecnica.robin.academia.ejb.CursoAcademiaService;
import autentia.pruebatecnica.robin.academia.http.FicheroHttp;
import autentia.pruebatecnica.robin.academia.http.FicheroHttpRequest;
import autentia.pruebatecnica.robin.academia.http.handler.CursoFicheroHttpRequestHandlerImpl;
import autentia.pruebatecnica.robin.academia.pojo.Curso;
import autentia.pruebatecnica.robin.academia.view.CursoView;

@RestController
@RequestMapping("/curso")
public class CursoAcademiaRestService {
	
	@Autowired
	private CursoAcademiaService cursoService;
	@Autowired
	private Environment env;
	@Autowired
	private CursoJsonDtoParser cursoDtoParser;
	@Autowired
	private CursoFicheroHttpRequestHandlerImpl cursoHttphandler;
	
	@GetMapping("/findById/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Curso findActiveById(@PathVariable("id") Long id) {
		return cursoService.findActiveById(id);
	}
	
	@GetMapping("/findAllCursosActivosPaginated")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Curso> findAllActivePaginatedSorted(@QueryParam("offset") Integer offset, @QueryParam("limit") Integer limit, @QueryParam("sorting") String sorting) {
		
		RowBounds opt = Optional.ofNullable(processRowBoundsQueryParams(offset, limit)).orElseGet(() -> new RowBounds());
		sorting = Optional.ofNullable(processSortingQueryParam(sorting)).orElseGet(() -> env.getProperty(AcademiaApplication.DEFAULT_CURSO_SORTING_ORDER));
				
		return this.cursoService.findAllActivePaginatedSorted(opt, sorting);
	}
	
	@GetMapping("/findViewAllActivePaginatedSorted")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CursoView> findViewAllActivePaginatedSorted(@QueryParam("offset") Integer offset, @QueryParam("limit") Integer limit, @QueryParam("sorting") String sorting) {
		
		RowBounds opt = Optional.ofNullable(processRowBoundsQueryParams(offset, limit)).orElseGet(() -> new RowBounds());
		sorting = Optional.ofNullable(processSortingQueryParam(sorting)).orElseGet(() -> env.getProperty(AcademiaApplication.DEFAULT_CURSO_SORTING_ORDER));
				
		return this.cursoService.findViewAllActivePaginatedSorted(opt, sorting);
	}
	
	@PostMapping("/insertCursoAutoId")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Long insertCursoAutoId(@RequestBody CursoJsonDTO curso) {
		return cursoService.insertCursoAutoId(this.cursoDtoParser.parseDto(curso));
	}
	
	

	@RequestMapping(value = "/insertCursoAutoIdUploadFile", 
			method = RequestMethod.POST,
			produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Long insertCursoAutoIdWithFile(@RequestParam("curso") String json, 
											@RequestParam("file") MultipartFile file) {
		
		return cursoService.insertCursoAutoId(
				processMultipartCursoInsert(json, file));
	}
	
	
	
	@PutMapping("/updateCurso")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Long updateCurso(@RequestBody CursoJsonDTO curso) {
		return cursoService.updateCurso(this.cursoDtoParser.parseDto(curso));
	}
	
	@DeleteMapping("/deleteById/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Long deleteById(@PathVariable("id") Long id) {
		return cursoService.deleteById(id);
	}
	
	public Curso processMultipartCursoInsert(String json, MultipartFile detalleFichero) {
		
		FicheroHttp ficheroHttp = null;
		CursoJsonDTO curso = null;
		
		try {
			ficheroHttp = new FicheroHttp(detalleFichero.getName(), detalleFichero.getOriginalFilename(), 
						detalleFichero.getSize(), detalleFichero.getInputStream());
			ObjectMapper om = new ObjectMapper();
			curso = om.readValue(json, CursoJsonDTO.class);
		} catch (JsonMappingException e) {
			throw new RuntimeException("Exception parseando el String a CursoJsonDTO", e);
		} catch (Exception e) {
			throw new RuntimeException("Exception tratando el fichero subido en el alta de un curso", e);
		}
		
		return this.cursoHttphandler.handle(new FicheroHttpRequest(
											curso, 
											ficheroHttp));
	}
	
	@GetMapping("/descargarCursoTemario/{idCurso}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public byte[] downloadFile(@PathVariable("idCurso") Long id) {
		return this.cursoService.downloadFile(id);
	}
	
	private String processSortingQueryParam(String sorting) {
		return cursoService.validateSortingValue(sorting);
	}

	public RowBounds processRowBoundsQueryParams(Integer offset, Integer limit) {
		return cursoService.validateRowBoundsValues(offset, limit);
	}

	public CursoAcademiaService getCursoService() {
		return cursoService;
	}

	public void setCursoService(CursoAcademiaService cursoService) {
		this.cursoService = cursoService;
	}
	
	

}
