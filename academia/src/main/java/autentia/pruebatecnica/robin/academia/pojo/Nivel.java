package autentia.pruebatecnica.robin.academia.pojo;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Entity;

import org.apache.ibatis.type.Alias;

@Alias("nivel")
@Entity
public class Nivel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String descripcion;
	
	public Nivel() {
		
	}

	public Nivel(Long id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public String toString() {
		return "Nivel [id=" + id + ", descripcion=" + descripcion + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(descripcion, id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Nivel)) {
			return false;
		}
		Nivel other = (Nivel) obj;
		return Objects.equals(descripcion, other.descripcion) && Objects.equals(id, other.id);
	}
}
