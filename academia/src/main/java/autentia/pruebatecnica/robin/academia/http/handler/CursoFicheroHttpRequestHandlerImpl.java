package autentia.pruebatecnica.robin.academia.http.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import autentia.pruebatecnica.robin.academia.dtoParsers.CursoJsonDtoParser;
import autentia.pruebatecnica.robin.academia.http.FicheroHttpRequest;
import autentia.pruebatecnica.robin.academia.pojo.Curso;
import ch.qos.logback.core.util.CloseUtil;

@Component
public class CursoFicheroHttpRequestHandlerImpl implements FicheroHttpRequestHandler<Curso> {

	@Autowired
	private CursoJsonDtoParser cursoJsonParser;
	
	@Override
	public Curso handle(FicheroHttpRequest request) {
		
		Curso c = cursoJsonParser.parseDto(request.getCurso());
		
		if (request.getFichero().getFichero()!=null) {
			byte[] data = null;
			
			try {
				 data = new byte[request.getFichero().getFichero().available()];
				 request.getFichero().getFichero().read(data);
				 c.setTemario(data);
			} catch (Exception e) {
				throw new RuntimeException("Error leyendo el temario de un curso", e);
			} finally {
				CloseUtil.closeQuietly(request.getFichero().getFichero());
			}
		}
		
		return c;
	}

}
