package autentia.pruebatecnica.robin.academia.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import autentia.pruebatecnica.robin.academia.dtoParsers.ProfesorJsonDtoParser;
import autentia.pruebatecnica.robin.academia.ejb.NivelAcademiaService;
import autentia.pruebatecnica.robin.academia.pojo.Nivel;

@RestController
@RequestMapping("/nivel")
public class NivelAcademiaRestService {

	@Autowired
	private NivelAcademiaService nivelService;
	/**
	 * para parsear @Requestbody a NivelJsonDTO
	 */
	@Autowired
	private ProfesorJsonDtoParser nivelJsonParser;
	
	@GetMapping("/findAll")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Nivel> findAll() {
		return this.nivelService.findAll();
	}

	@GetMapping("/findById/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Nivel findById(@PathVariable("id") Long id) {
		return this.nivelService.findById(id);
	}
	
}
