insert into nivel (id, descripcion) values (1, 'basico');
insert into nivel (id, descripcion) values (2, 'intermedio');
insert into nivel (id, descripcion) values (3, 'avanzado');

insert into profesor (id, nombre, apellidos) values (1, 'juan', 'mj');
insert into profesor (id, nombre, apellidos) values (2, 'paco', 'sn');
insert into profesor (id, nombre, apellidos) values (3, 'antonio', 'lk');
insert into profesor (id, nombre, apellidos) values (4, 'miguel', 'mc');
insert into profesor (id, nombre, apellidos) values (5, 'pedro', 'cj');
insert into profesor (nombre, apellidos) values ('manu', 'lp');

insert into curso (id, activo, titulo, horas, nivel, profesor) values (1, true, 'curso 1', 5, 1, 4);
insert into curso (id, activo, titulo, horas, nivel, profesor) values (2, true, 'curso 2', 58, 2, 5);
insert into curso (id, activo, titulo, horas, nivel, profesor) values (3, true, 'curso 3', 21, 1, 4);
insert into curso (id, activo, titulo, horas, nivel, profesor) values (4, true, 'curso 4', 10, 1, 1);
insert into curso (id, activo, titulo, horas, nivel, profesor) values (5, true, 'curso 5', 3, 1, 2);
insert into curso (id, activo, titulo, horas, nivel, profesor) values (6, true, 'curso 6', 500, 3, 5);
insert into curso (id, activo, titulo, horas, nivel, profesor) values (7, false, 'curso 7', 30, 1, 1);
insert into curso (id, activo, titulo, horas, nivel, profesor) values (8, true, 'curso 8', 20, 1, 2);
insert into curso (id, activo, titulo, horas, nivel, profesor) values (9, true, 'curso 9', 25, 1, 1);
insert into curso (id, activo, titulo, horas, nivel, profesor) values (10, false, 'curso 10', 70, 3, 2);

commit;