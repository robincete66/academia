DROP TABLE curso if EXISTS;
DROP TABLE profesor if EXISTS;
DROP TABLE nivel if EXISTS;
create table if not exists nivel
(
   id IDENTITY, 
   descripcion varchar(255) not null
);
create table if not exists profesor
(
   id IDENTITY,
   nombre varchar(50) not null,
   apellidos varchar(100)
);
create table if not exists curso
(
   id IDENTITY,
   activo boolean not null, 
   titulo varchar(255) not null,
   horas int not null,
   nivel int not null,
   profesor int not null,
   temario blob,
   foreign key (nivel)
   references nivel(id),
   foreign key (profesor)
   references profesor(id)
);